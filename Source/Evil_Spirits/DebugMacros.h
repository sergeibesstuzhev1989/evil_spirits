﻿#pragma once

#include "DrawDebugHelpers.h"

// макросы сфера, линия, точка, вектор
#define DRAW_SPHERE(Location) if(GetWorld()) DrawDebugSphere(GetWorld(), Location, 25.f , 24, FColor::Blue, true);
#define DRAW_SPHERE_COLORE(Location, Colore) if(GetWorld()) DrawDebugSphere(GetWorld(), Location, 10.f, 15, Colore, \
		false, 5.f);
#define DRAW_SPHERE_SingleFrame(Location) if(GetWorld()) DrawDebugSphere(GetWorld(), Location, 80.f , 24, FColor::Purple,\
		false, -1.f);
#define DRAW_COLOR_SPHERE(Location, Colore) if(GetWorld()) DrawDebugSphere(GetWorld(), Location, 5.f, 15, Colore, true);
#define DRAW_LINE(StartLocation, EndLocation) if(GetWorld()) DrawDebugLine(GetWorld(), StartLocation, EndLocation, \
FColor::Emerald, true, -1.f, 0, 1.f);
#define DRAW_LINE_SingleFrame(StartLocation, EndLocation) if(GetWorld()) DrawDebugLine(GetWorld(), StartLocation, \
		EndLocation, FColor::Emerald, false, -1.f, 0, 1.f);
#define DRAW_POINT(Location) if(GetWorld()) DrawDebugPoint(GetWorld(), EndLocation, 80.f, FColor::Purple, true);
#define DRAW_POINT_SingleFrame(Location) if(GetWorld()) DrawDebugPoint(GetWorld(), EndLocation, 80.f, \
		FColor::Purple, false, -1.f);
#define DRAW_VECTOR(StartLocation, EndLocation) if(GetWorld()) \
{ \
DrawDebugLine(GetWorld(), StartLocation, EndLocation, FColor::Emerald, true, -1.f, 0, 1.f); \
DrawDebugPoint(GetWorld(), EndLocation, 30.f, FColor::Purple, true); \
}
#define DRAW_VECTOR_SingleFrame(StartLocation, EndLocation) if(GetWorld()) \
{ \
DrawDebugLine(GetWorld(), StartLocation, EndLocation, FColor::Black, false, -1.f, 0, 1.f); \
DrawDebugPoint(GetWorld(), EndLocation, 30.f, FColor::Yellow, false, -1.f); \
}