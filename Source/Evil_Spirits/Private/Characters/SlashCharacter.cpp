// Fill out your copyright notice in the Description page of Project Settings.

#include "Characters/SlashCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GroomComponent.h"
//#include "Items/Item.h"
#include "CharacterTypes.h"
#include "Items/Weapons/Weapon.h"
#include "Animation/AnimMontage.h"
#include "Components/BoxComponent.h"

// Sets default values
ASlashCharacter::ASlashCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GetCharacterMovement()->bOrientRotationToMovement = true; // персонаж разворачивается по направлению движения
	GetCharacterMovement()->RotationRate = FRotator(0.f, 400.f, 0.f);

	// автоматическое снятие галочек на вращение персонажа от кнопок
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// каждый раз при создании переменной
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetupAttachment(GetRootComponent());
	SpringArm->TargetArmLength = 300.f; // дефолтное расстояние от камеры до чара

	// сама камера 
	ViewCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("ViewCamera"));
	ViewCamera->SetupAttachment(SpringArm); // чтобы все двигалось вместе с корневым компонентом надо приатачивать

	// автоматическое приклепление игрока к персу
	AutoPossessPlayer = EAutoReceiveInput::Player0;

	// конструируем волосы и брови
	Hair = CreateDefaultSubobject<UGroomComponent>(TEXT("Hair"));
	Hair->SetupAttachment(GetMesh());
	Hair->AttachmentName = FString("head");

	Eyebrows = CreateDefaultSubobject<UGroomComponent>(TEXT("EyeBrows"));
	Eyebrows->SetupAttachment(GetMesh());
	Eyebrows->AttachmentName = FString("head");
}

void ASlashCharacter::SetWeaponCollisionEnabled(ECollisionEnabled::Type CollisionEnabled)
{
	if(EquippedWeapon && EquippedWeapon->GetWeaponBox())
	{
		EquippedWeapon->GetWeaponBox()->SetCollisionEnabled(CollisionEnabled);
		EquippedWeapon->IgnoreActors.Empty();
	}
}

// Called when the game starts or when spawned
void ASlashCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

void ASlashCharacter::MoveForvard(float Value)
{
	//UE_LOG(LogTemp, Warning, TEXT("Value: %f"), Value);
	if(ActionState != EActionState::EAS_Unoccupied) return; // во время атаки нельзя двигаться по сторонам
	if(Controller && (Value != 0.f))
	{
		// выясняем направление вперед
		const FRotator ControlRotation = GetControlRotation();
		const FRotator YawRotation(0.f, ControlRotation.Yaw, 0.f);

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ASlashCharacter::Turn(float Value)
{
	AddControllerYawInput(Value);
}

void ASlashCharacter::LookUp(float Value)
{
	//UE_LOG(LogTemp, Warning, TEXT("Value: %f"), Value);
	AddControllerPitchInput(Value);
}

void ASlashCharacter::MoveRight(float Value)
{
	if(ActionState != EActionState::EAS_Unoccupied) return; // во время атаки нельзя двигаться по сторонам
	if(Controller && (Value != 0.f))
	{
		// ищем направление в право
		const FRotator ControlRotation = GetControlRotation();
		const FRotator YawRotation(0.f, ControlRotation.Yaw, 0.f);

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(Direction, Value);
	}
}

void ASlashCharacter::FKeyPressed()
{
	AWeapon* OverlappingWeapon = Cast<AWeapon>(OverlappingItem);
	if(OverlappingWeapon /*&& CharacterState == ECharacterState::ECS_Unequipped &&
		RemovedState == EWeaponRemovedState::EWRS_Empty*/)
	{
		//if(GEngine) GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Emerald, "PickUp weapon");
		OverlappingWeapon->Equip(GetMesh(), FName("RightHandSocket"), this, this);
		CharacterState = ECharacterState::ECS_EquippedOneHandedWeapon;
		OverlappingItem = nullptr;
		//RemovedState = EWeaponRemovedState::EWRS_Taken;
		EquippedWeapon = OverlappingWeapon;
	}
	else 
	{
		if(CanDisarm())
		{
			//if(GEngine) GEngine->AddOnScreenDebugMessage(2, 5.f, FColor::Emerald, "Unequip");
			PlayEquipMontage(FName("Unequip"));
			CharacterState = ECharacterState::ECS_Unequipped;
			//RemovedState = EWeaponRemovedState::EWRS_Removed;
			ActionState = EActionState::EAS_EquippingWeapon;
		}
		else if(CanArm())
		{
			//if(GEngine) GEngine->AddOnScreenDebugMessage(3, 5.f, FColor::Emerald, "Equip");
			PlayEquipMontage(FName("Equip"));
			CharacterState = ECharacterState::ECS_EquippedOneHandedWeapon;
			//RemovedState = EWeaponRemovedState::EWRS_Taken;
			ActionState = EActionState::EAS_EquippingWeapon;
		}
	}
}

void ASlashCharacter::Attack()
{
	if(CanAttack())
	{
		//if(GEngine) GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Green, "Can Attack");
		PlayAttackMontage();
		ActionState = EActionState::EAS_Attacking;
	}
	
}

bool ASlashCharacter::CanAttack()
{
	//if(GEngine) GEngine->AddOnScreenDebugMessage(2, 5.f, FColor::Red, "Cant Attack");
	return ActionState == EActionState::EAS_Unoccupied &&
			CharacterState != ECharacterState::ECS_Unequipped;
}

bool ASlashCharacter::CanDisarm()
{
	return ActionState == EActionState::EAS_Unoccupied &&
			CharacterState != ECharacterState::ECS_Unequipped &&
			//RemovedState == EWeaponRemovedState::EWRS_Taken &&
			EquippedWeapon;
}

bool ASlashCharacter::CanArm()
{
	return ActionState == EActionState::EAS_Unoccupied &&
			CharacterState == ECharacterState::ECS_Unequipped &&
			//RemovedState == EWeaponRemovedState::EWRS_Removed &&
			EquippedWeapon;
}

void ASlashCharacter::Disarm()
{
	if(EquippedWeapon)
	{
		EquippedWeapon->AttachMeshToSocked(GetMesh(), FName("SpineSocket"));
	}
}

void ASlashCharacter::Arm()
{
	if(EquippedWeapon)
	{
		EquippedWeapon->AttachMeshToSocked(GetMesh(), FName("RightHandSocket"));
	}
}

void ASlashCharacter::FinishEquipping()
{
	ActionState = EActionState::EAS_Unoccupied;
}

void ASlashCharacter::PlayAttackMontage()
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if(AnimInstance && AttackMontage)
	{
		AnimInstance->Montage_Play(AttackMontage);
		const int32 Selection = FMath::RandRange(0, 1); // на случай нескольких анимаций
		FName SectionName = FName();
		switch(Selection)
		{
		case 0:
			SectionName = FName("Attack1");
			break;
		case 1:
			SectionName = FName("Attack2");
			break;
		default:
			break;
		}
		AnimInstance->Montage_JumpToSection(SectionName, AttackMontage);
	}
}

void ASlashCharacter::PlayEquipMontage(const FName& SectionName)
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if(AnimInstance && EquipMontage)
	{
		AnimInstance->Montage_Play(EquipMontage);
		AnimInstance->Montage_JumpToSection(SectionName, EquipMontage);
	}
}

void ASlashCharacter::AttackEnd()
{
	//if(GEngine) GEngine->AddOnScreenDebugMessage(3, 5.f, FColor::Black, "End Attack");
	ActionState = EActionState::EAS_Unoccupied;
}

// Called every frame
void ASlashCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASlashCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// после создания и инициализации ввода, надо забиндить кнопки
	PlayerInputComponent->BindAxis(FName("MoveForvard"), this, &ASlashCharacter::MoveForvard);
	PlayerInputComponent->BindAxis(FName("Turn"), this, &ASlashCharacter::Turn);
	PlayerInputComponent->BindAxis(FName("LookUp"), this, &ASlashCharacter::LookUp);
	PlayerInputComponent->BindAxis(FName("MoveRight"), this, &ASlashCharacter::MoveRight);

	PlayerInputComponent->BindAction(FName("Jump"), IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction(FName("Equip"), IE_Pressed, this, &ASlashCharacter::FKeyPressed);
	PlayerInputComponent->BindAction(FName("Attack"), IE_Pressed, this, &ASlashCharacter::Attack);

}

